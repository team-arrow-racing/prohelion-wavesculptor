//! Battery management system driver.
//!
//! [Product page](https://www.prohelion.com/product-category/bms/)
//! [User's manual](https://www.prohelion.com/wp-content/uploads/2022/07/PHLN67.011v2-BMS-Users-Manual.pdf)
